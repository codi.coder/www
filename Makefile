.PHONY: install
install:
	bundle install

.PHONY: build
build:
	bundle exec jekyll build -d public

.PHONY: serve
serve:
	bundle exec jekyll serve
