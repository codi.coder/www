---
name: Messagerie instantannée
subtitle: "Bientôt : La messagerie"
order: 5
external_name: messagerie
external_url: https://rocket.interfoodcoop.net/
image_path: /images/services/rocket.png
ref: messagerie
lang: fr
---
